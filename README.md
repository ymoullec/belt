# UI fields and buttons

- Belt parameters:
  - Port: e.g. COM3, check your peripherals in the peripheral manager or use the Dynamixel Wizard to know the port that the belt uses
  - Band length: length of the elastic band you are using on the belt
  - Band thickness: thickness of the elastic band you are using on the belt
  - Reconnect button: green if belt is connected, red if not, click it to reconnect, click it also to apply the changes made to length or thickness if the belt was connected when making the changes
- Commands:
  - Set band button: wraps the band until it reaches a given tightness (weak) so it sits on the user's body
  - Start breathing button: starts rendering a breathing pattern with the belt (the strength is controlled by the Strength slider and parameters of the Motor controller, the duration is controlled by parameters in the Breath controller)
  - Stop breathing button: stops the breathing pattern
  - Wrap button: wraps the band once (the strength is controlled by the Strength slider and parameters of the Motor controller, the duration is controlled by parameters in the Wrap unwrap component)
  - Unwrap button: unwrap the band once
  - Strength slider: tightness of the band when wrapped completely (see parameters in Motor controller)
  - Quit button: quits the app

# Editor parameters

All the parameters are found in components of the SCRIPT GameObject:
- Breath controller:
  - Period: period of the breathing cycle in seconds
- Wrap unwrap component:
  - Duration: duration of the wrap / unwrap
- Motor controller component:
  - Goal current set: current the motor needs to reach to set the band (50 works)
  - Goal current wrapped min: current the motor needs to reach to wrap the band when the strength slider is at min
  - Goal current wrapped max: current the motor needs to reach to wrap the band when the strength slider is at max
    - Should be: Set < Min < Max
    - Min and max can be changed to reach the desired tightness (note that the motor has a relatively low maximum velocity and can thus saturate, velocity is then clamped and a warning is printed into the console)
  - Verbose: can be toggled to print more debug messages into the console
  - Baudrate, debug: should not be changed
  - Is band set, Is belt connected: read-only
- Init component: nothing to customize