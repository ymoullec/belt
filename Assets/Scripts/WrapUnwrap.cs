using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WrapUnwrap : MonoBehaviour
{
    public float duration = 1;

    public Slider strength;
    public MotorController belt;

    public void Wrap()
    {
        belt.TriggerWrap(duration, strength.value);
    }

    public void Unwrap()
    {
        belt.TriggerUnwrap(duration);
    }
}
