using dynamixel_sdk;
using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class MotorThread
{
    // State machine
    // None -(S)-> SetBand --> None -(W)-> Wrap --> None -(U)-> Unwrap -
    //                           ^                                      |
    //                           |______________________________________|
    public enum Mode { Idle, SetBand, Wrap, UnWrap, SetPosition };

    #region Control table address
    public const int ADDR_OPERATING_MODE = 11;

    public const int ADDR_MAX_VOLTAGE = 32;
    public const int ADDR_MIN_VOLTAGE = 34;
    public const int ADDR_PWM_LIMIT = 36;
    public const int ADDR_CURRENT_LIMIT = 38;
    public const int ADDR_VELOCITY_LIMIT = 44;
    public const int ADDR_MAX_POSITION = 48;
    public const int ADDR_MIN_POSITION = 52;

    public const int ADDR_TORQUE_ENABLE = 64;

    public const int ADDR_GOAL_PWM = 100;
    public const int ADDR_GOAL_CURRENT = 102;
    public const int ADDR_GOAL_VELOCITY = 104;
    public const int ADDR_PROFILE_ACCELERATION = 108;
    public const int ADDR_PROFILE_VELOCITY = 112;
    public const int ADDR_GOAL_POSITION = 116;

    public const int ADDR_PRESENT_PWM = 124;
    public const int ADDR_PRESENT_CURRENT = 126;
    public const int ADDR_PRESENT_VELOCITY = 128;
    public const int ADDR_PRESENT_POSITION = 132;
    #endregion

    #region Settings
    private byte ID;
    private int BAUDRATE;
    public const int TORQUE_ENABLE = 1;                 // Value for enabling the torque
    public const int TORQUE_DISABLE = 0;                // Value for disabling the torque
    public const int DXL_MOVING_STATUS_THRESHOLD = 20;  // Dynamixel moving status threshold
    #endregion

    #region Additional constants
    // Communication
    public const int COMM_SUCCESS = 0;              // Communication Success result value
    public const int COMM_TX_FAIL = -1001;          // Communication Tx Failed

    // Protocol version
    public const int PROTOCOL_VERSION = 2;          // See which protocol version is used in the Dynamixel
    public const byte ESC_ASCII_VALUE = 0x1b;
    #endregion

    #region Global variables
    // DXL
    private int dxl_comm_result;
    private int dxl_operating_mode;
    private int dxl_goal_current;
    private int dxl_goal_velocity;
    private long dxl_goal_position;
    private byte dxl_error;
    private byte dxl_present_mode;
    private Int16 dxl_present_current;
    private Int32 dxl_present_velocity;
    private Int32 dxl_present_position;

    // Belt
    private const double R0 = 0.045 / 2;    // Cylinder radius (m)

    // Motor
    private const double kcL0 = 0.047;            // Link current <-> normalized angular position (m * c_inc / p_inc)
    private const double kv = 0.229;              // Speed per motor increment (rpm / v_inc)
    private const double kp = 0.088;              // Angle per motor increment (° / p_inc)

    // Band
    private int portNum;                    // Port number
    private double bandLength = 1.41;       // Band length (m)
    private double bandThickness = 0.001;   // Band thickness (m)
    private double L0;                      // Length when set on user (m)

    // Runtime states
    public bool isActive = true;            // 0. Controller is active (false when exit routine)
    public bool isBeltConnected = false;    // 0. Belt is connected
    public bool isMotorInBegPos = false;    // 1. Motor needs to get in beginning position
    public bool isBandSet = false;          // 2. Band needs to be set on user before wrapping
    public bool readyToRecCommands = false; // 3. Band can receive commands
    private Mode mode = Mode.Idle;          // Controller state
    private int avoidStart = 0;             // Avoid stopping motor because high current at start

    // Runtime variables
    private long begPosition = 0;           // Angular position before setting band (p_inc)
    private long setPosition = 0;           // Angular position when set (p_inc)
    private double duree;                   // Duration of the current phase (s)
    #endregion

    #region Thread parameters
    private bool stopRequested = false;
    private Queue msgQ;
    #endregion

    #region Constructor parameters
    private string port;
    private bool verbose;
    private int goalCurrentSet;
    private int goalCurrentWrappedMin;
    private int goalCurrentWrappedMax;
    #endregion

    #region Init loop
    private bool isPortOpened = false;
    private bool isBaudRateSet = false;
    private float delayFail = 0.5f;
    #endregion

    Stopwatch checkDuration = new Stopwatch();    // Chronomètre

    public MotorThread(
                        string port,
                        float bandLength,
                        float bandThickness,
                        byte id,
                        int baudrate,
                        bool verbose,
                        int goalCurrentSet,
                        int goalCurrentWrappedMin,
                        int goalCurrentWrappedMax)
    {
        this.port = port;
        this.bandLength = bandLength;
        this.bandThickness = bandThickness;
        this.ID = id;
        this.BAUDRATE = baudrate;
        this.verbose = verbose;
        this.goalCurrentSet = goalCurrentSet;
        this.goalCurrentWrappedMin = goalCurrentWrappedMin;
        this.goalCurrentWrappedMax = goalCurrentWrappedMax;

        msgQ = Queue.Synchronized(new Queue());
    }

    public struct CommandMsg
    {
        public char code;
        public double dura;
        public float inte;
        public CommandMsg(char code)
        {
            this.code = code;
            dura = 0;
            inte = 0;
        }
        public CommandMsg(char code, double dura)
        {
            this.code = code;
            this.dura = dura;
            inte = 0;
        }
        public CommandMsg(char code, double dura, float inte)
        {
            this.code = code;
            this.dura = dura;
            this.inte = inte;
        }
    }

    /// <summary>
    /// Stop thread from script
    /// </summary>
    public void RequestStop()
    {
        lock (this)
        {
            stopRequested = true;
        }
    }

    /// <summary>
    /// Called once to loop for entire simulation
    /// </summary>
    public void RunForever()
    {
        while (!IsStopRequested())
        {
            Initialize();

            while (!IsStopRequested())
                RunOnce();
        }

        CloseDevice();
    }

    /// <summary>
    /// Enqueue new CommandMsg in MsgQ, message will be treated in RunForever
    /// </summary>
    /// <param name="cmd">Command (code == 'E'||'S'||'U'||'W', duration, intensity)</param>
    public void SendMessage(CommandMsg cmd) { msgQ.Enqueue(cmd); }

    /// <summary>
    /// Is stop requested ?
    /// </summary>
    /// <returns>True if stop is requested</returns>
    private bool IsStopRequested() { lock (this) { return stopRequested; } }

    /// <summary>
    /// Called once at start of thread
    /// </summary>
    private void Initialize()
    {
        // Get methods and members of PortHandlerWindows
        portNum = Dynamixel.portHandler(port);

        // Initialize PacketHandler Structs
        Dynamixel.packetHandler();

        //index = 0;
        dxl_comm_result = COMM_TX_FAIL;     // Communication result
        dxl_error = 0;                      // Dynamixel error
        dxl_present_position = 0;           // Present position

        // Open port 
        OpenPort();
    }

    /// <summary>
    /// Open port and set baudrate if possible
    /// </summary>
    private void OpenPort()
    {
        // Open port
        if (Dynamixel.openPort(portNum))
        {
            if (verbose) UnityEngine.Debug.Log("BELT : Suceeded to open port");
            isPortOpened = true;

            SetBaudRate();
        }
        else
        {
            UnityEngine.Debug.LogError("BELT : Failed to open port");
            checkDuration = Stopwatch.StartNew();
        }
    }

    /// <summary>
    /// Set baud rate
    /// </summary>
    private void SetBaudRate()
    {
        // Set port baudrate
        if (Dynamixel.setBaudRate(portNum, BAUDRATE))
        {
            if (verbose) UnityEngine.Debug.Log("BELT : Succeeded to set the baudrate");
            isBaudRateSet = true;
            isBeltConnected = true;

            // Beg position (before setting band)
            if (!isBandSet)
            {
                TriggerSetPosition(1024);
                SetVelocity(0);
                L0 = bandLength;
            }
            // Belt has disconnected: put it back in set position 
            else
            {
                TriggerSetPosition(setPosition);
            }
        }
        else
        {
            UnityEngine.Debug.LogError("BELT : Failed to set the baudrate");
            checkDuration = Stopwatch.StartNew();
        }
    }

    /// <summary>
    /// Called once while loop iteration
    /// </summary>
    private void RunOnce()
    {
        // Open port and set baud rate if not already done
        if (!isPortOpened && checkDuration.ElapsedMilliseconds > delayFail * 1000)
        {
            checkDuration.Stop();
            OpenPort();
        }
        if (isPortOpened && !isBaudRateSet && checkDuration.ElapsedMilliseconds > delayFail * 1000)
        {
            checkDuration.Stop();
            SetBaudRate();
        }

        // Consume commands
        if (readyToRecCommands)
        {
            while (msgQ.Count != 0 && readyToRecCommands)
            {
                CommandMsg m = (CommandMsg)msgQ.Dequeue();
                switch (m.code)
                {
                    case 'R':
                        UnityEngine.Debug.Log("BELT : Reset position");
                        TriggerResetPos();
                        break;
                    case 'S':
                        UnityEngine.Debug.Log("BELT : Setting belt");
                        TriggerSetBand();
                        break;
                    case 'W':
                        TriggerWrap(m.dura, m.inte);
                        break;
                    case 'U':
                        TriggerUnwrap(m.dura);
                        break;
                    case 'E':
                        UnityEngine.Debug.Log("BELT : Emergency stop");
                        Emergency();
                        break;
                }
            }
        }

        // Execute command (mod is changed in "Trigger" functions)
        if (mode == Mode.SetBand) SetBand();
        else if (mode == Mode.Wrap) Wrap();
        else if (mode == Mode.UnWrap) Unwrap();
        else if (mode == Mode.SetPosition) SetPosition(dxl_goal_position);
    }

    /// <summary>
    /// Called once at end of component lifecycle
    /// </summary>
    private void CloseDevice()
    {
        // Disable Dynamixel Torque
        DisableTorque();

        // Close port
        Dynamixel.closePort(portNum);
        if (verbose) UnityEngine.Debug.Log("BELT : Dynamixel has been disconnected");
    }

    /// <summary>
    /// In case of emergency, belt unwraps and cannot wrap anymore
    /// </summary>
    public void Emergency()
    {
        readyToRecCommands = false;
        isActive = false;
        isBandSet = false;
        TriggerSetPosition(1024);
    }

    /// <summary>
    /// Called when communication error
    /// </summary>
    private void OnCommError()
    {
        if (isPortOpened)
        {
            UnityEngine.Debug.LogError("BELT : Disconnected, will try to reconnect after delay");
            Dynamixel.closePort(portNum);
            isPortOpened = false;
            isBaudRateSet = false;
            isBeltConnected = false;
            readyToRecCommands = false;
            checkDuration = Stopwatch.StartNew();
        }
    }

    /// <summary>
    /// Switch to SetPosition state
    /// </summary>
    /// <param name="position"></param>
    private void TriggerSetPosition(long position)
    {
        dxl_goal_position = position;
        mode = Mode.SetPosition;
        SetMode(4);
    }

    /// <summary>
    /// Reset motor position
    /// </summary>
    private void TriggerResetPos()
    {
        isBandSet = false;
        TriggerSetPosition(1024);
        SetVelocity(0);
        L0 = bandLength;
    }

    /// <summary>
    /// Switch to SetBand state
    /// </summary>
    private void TriggerSetBand()
    {
        isBandSet = false;
        isMotorInBegPos = false;
        L0 = bandLength;
        mode = Mode.SetBand;
        SetMode(1);

        // Initialize mode parameters
        ReadCurrent();
        avoidStart = 0;
    }

    /// <summary>
    /// Switch to Wrap state
    /// </summary>
    /// <param name="duration">Duration in s</param>
    /// <param name="intensity">Intensity (0-1)</param>
    private void TriggerWrap(double duration, float intensity)
    {
        isMotorInBegPos = false;
        mode = Mode.Wrap;
        SetMode(1);
        ReadCurrent();
        ReadPosition();

        // Compute command
        // Take current position into account in order not to wrap too much
        duree = duration;
        dxl_goal_current = (int)Mathf.Clamp(
            goalCurrentWrappedMin + intensity * (goalCurrentWrappedMax - goalCurrentWrappedMin),
            goalCurrentWrappedMin,
            goalCurrentWrappedMax);
        double delta_position = L0 * dxl_goal_current / kcL0 + setPosition - dxl_present_position;
        dxl_goal_velocity = (int)Math.Round(60 * kp * delta_position / (360 * kv * duree));

        // Motor saturates at velocity 200
        if (dxl_goal_velocity > 200)
        {
            UnityEngine.Debug.LogWarning("BELT Wrap : Engine saturation, dxl_goal_velocity = " + dxl_goal_velocity);
            dxl_goal_velocity = 200;
        }
        else if (dxl_goal_velocity < -200)
        {
            UnityEngine.Debug.LogWarning("BELT Wrap : Engine saturation, dxl_goal_velocity = " + dxl_goal_velocity);
            dxl_goal_velocity = -200;
        }
        else if (verbose) UnityEngine.Debug.Log("BELT Wrap : dxl_goal_velocity = " + dxl_goal_velocity);

        // Start stopwatch
        checkDuration = Stopwatch.StartNew();
    }

    /// <summary>
    /// Switch to Unwrap state
    /// </summary>
    /// <param name="duration">Duration in s</param>
    private void TriggerUnwrap(double duration)
    {
        isMotorInBegPos = false;
        mode = Mode.UnWrap;
        SetMode(1);
        ReadPosition();

        // Compute command
        duree = duration;
        double delta_position = (setPosition - dxl_present_position);
        dxl_goal_velocity = (int)Math.Round(60 * kp * delta_position / (360 * kv * duree));

        // Motor saturates at velocity 200
        if (dxl_goal_velocity < -200)
        {
            UnityEngine.Debug.LogWarning("BELT Unwrap : Engine saturation, dxl_goal_velocity = " + dxl_goal_velocity);
            dxl_goal_velocity = -200;
        }
        else if (dxl_goal_velocity > 200)
        {
            UnityEngine.Debug.LogWarning("BELT Unwrap : Engine saturation, dxl_goal_velocity = " + dxl_goal_velocity);
            dxl_goal_velocity = 200;
        }
        else if (verbose) UnityEngine.Debug.Log("BELT Unwrap : dxl_goal_velocity = " + dxl_goal_velocity);

        // Start stopwatch
        checkDuration = Stopwatch.StartNew();
    }

    /// <summary>
    /// Set band to reach goalCurrentSet
    /// </summary>
    private void SetBand()
    {
        if (isActive)
        {
            // Wrap band until tight enough
            SetVelocity(100);
            ReadCurrent();

            // Avoid stopping because high current at start
            avoidStart = avoidStart + 1;

            if (dxl_present_current > goalCurrentSet && avoidStart > 5)
            {
                // Stop when current > 20 inc
                SetVelocity(0);
                ReadPosition();
                mode = Mode.Idle;

                // Save set position
                setPosition = dxl_present_position;

                // Compute
                // - 2048 = half a turn
                // - fHTurn = number of half turns (float)
                // - nHTurn = number of half turns (int)
                // - perLastHTurn = percentage of last half turn (float)
                float fHTurn = (setPosition - begPosition) / 2048f;
                float nHTurn = Mathf.Floor(fHTurn);
                float perLastHTurn = fHTurn - nHTurn;
                L0 = L0 - (nHTurn * Mathf.PI * (2 * R0 + (nHTurn - 1) * bandThickness) + 2 * Mathf.PI * perLastHTurn * (R0 + nHTurn * bandThickness));

                // Log
                UnityEngine.Debug.Log("BELT Set : L0 = " + L0 + ", set_position = " + setPosition);

                isBandSet = true;
            }
        }
    }

    /// <summary>
    /// Wrap band
    /// </summary>
    private void Wrap()
    {
        ReadCurrent();
        if (
            isActive &&
            dxl_present_current < dxl_goal_current &&
            checkDuration.ElapsedMilliseconds < duree * 1000)
        {
            // Wrap until reach target current or duration
            SetVelocity(dxl_goal_velocity);
            ReadCurrent();
        }
        else
        {
            SetVelocity(0);
            mode = Mode.Idle;
            checkDuration.Stop();
        }
    }

    /// <summary>
    /// Unwrap band
    /// </summary>
    private void Unwrap()
    {
        ReadPosition();
        if (isActive &&
            dxl_present_position > setPosition &&
            checkDuration.ElapsedMilliseconds < duree * 1000)
        {
            // Unwrap until reach targe position (setPosition) or duration
            SetVelocity(dxl_goal_velocity);
            ReadPosition();
        }
        else
        {
            SetVelocity(0);
            mode = Mode.Idle;
            checkDuration.Stop();
        }
    }

    private void SetMode(int mode)
    {
        DisableTorque();
        dxl_operating_mode = mode;      // Between 0 and 1024 ; Check  the datasheet
        Dynamixel.write1ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_OPERATING_MODE, (byte)dxl_operating_mode);
        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
        ReadMode();
        EnableTorque();
    }

    private void SetPosition(long position)
    {
        dxl_goal_position = position;   // Between 0 and 1024 ; Check  the datasheet
        ReadPosition();
        if ((Math.Abs(dxl_goal_position - dxl_present_position) > DXL_MOVING_STATUS_THRESHOLD))
        {
            Dynamixel.write4ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_GOAL_POSITION, (UInt32)dxl_goal_position);
            if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
                OnCommError();
            else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
                UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
        }
        else
        {
            if (!isActive)
            {
                isMotorInBegPos = true;
            }
            else if (!isBandSet)
            {
                begPosition = dxl_present_position;
                readyToRecCommands = true;
                isMotorInBegPos = true;
            }
        }
    }

    private void SetVelocity(int velocity)
    {
        dxl_goal_velocity = velocity;   // Between 0 and 1024 ; Check  the datasheet
        ReadVelocity();
        if ((Math.Abs(dxl_goal_velocity - dxl_present_velocity) > DXL_MOVING_STATUS_THRESHOLD))
        {
            Dynamixel.write4ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_GOAL_VELOCITY, (UInt32)dxl_goal_velocity);
            if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
                OnCommError();
            else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
                UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
        }
    }

    private void ReadMode()
    {
        dxl_present_mode = (byte)Dynamixel.read4ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_OPERATING_MODE);

        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
    }

    private void ReadCurrent()
    {
        dxl_present_current = (Int16)Dynamixel.read4ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_PRESENT_CURRENT);

        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
    }

    private void ReadVelocity()
    {
        dxl_present_velocity = (Int32)Dynamixel.read4ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_PRESENT_VELOCITY);

        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
    }

    private void ReadPosition()
    {
        dxl_present_position = (Int32)Dynamixel.read4ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_PRESENT_POSITION);

        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
    }

    private void EnableTorque()
    {
        Dynamixel.write1ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_TORQUE_ENABLE, TORQUE_ENABLE);
        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
        else if (verbose)
            UnityEngine.Debug.Log("BELT: Dynamixel has been successfully connected");
    }

    private void DisableTorque()
    {
        Dynamixel.write1ByteTxRx(portNum, PROTOCOL_VERSION, ID, ADDR_TORQUE_ENABLE, TORQUE_DISABLE);

        if ((dxl_comm_result = Dynamixel.getLastTxRxResult(portNum, PROTOCOL_VERSION)) != COMM_SUCCESS)
            OnCommError();
        else if ((dxl_error = Dynamixel.getLastRxPacketError(portNum, PROTOCOL_VERSION)) != 0)
            UnityEngine.Debug.LogError("BELT: " + Marshal.PtrToStringAnsi(Dynamixel.getRxPacketError(PROTOCOL_VERSION, dxl_error)));
    }
}
