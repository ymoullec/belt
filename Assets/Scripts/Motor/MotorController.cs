using System.Threading;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MotorController : MonoBehaviour
{
    #region Parameters
    [Tooltip("Dynamixel ID (1)")]
    public int BAUDRATE = 57600;
    [Tooltip("Allows to send Wrap/Unwrap command with F2/F3 when enabled")]
    public bool debug = false;
    [Tooltip("Less console logs when disabled")]
    public bool verbose = false;
    [Tooltip("Current in motor in Set position")]
    public int goalCurrentSet = 50;
    [Tooltip("Current in motor in Wrapped position at lowest intensity")]
    public int goalCurrentWrappedMin = 75;
    [Tooltip("Current in motor in Wrapped position at highest intensity")]
    public int goalCurrentWrappedMax = 100;

    [Tooltip("Read only : Is band set")]
    public bool isBandSet = false;
    public bool isBeltConnected = false;

    protected Thread thread;
    protected MotorThread motorThread;

    public Image reconnect;
    public InputField portField;
    public Image setBandImage;
    public Button setBandButton;
    public Button startBreathingButton;
    public Button stopBreathingButton;
    public Button wrapButton;
    public Button unwrapButton;
    #endregion

    /// <summary>
    /// Send command to thread when needed
    /// </summary>
    void Update()
    {
        // If band is set, make info available for init
        setBandButton.GetComponent<Image>().color = motorThread.isBandSet ? Color.green : Color.white;
        isBandSet = motorThread.isBandSet;
        isBeltConnected = motorThread.isBeltConnected;

        // End application if emergency set and motor stopped
        if (!motorThread.isActive && motorThread.isMotorInBegPos)
        {
            #if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
            #else
            Application.Quit();
            #endif
        }

        // UI
        reconnect.color = isBeltConnected ? Color.green : Color.red;
        setBandImage.color = isBeltConnected && isBandSet ? Color.green : Color.white;
        setBandButton.interactable = isBeltConnected;
        startBreathingButton.interactable = isBeltConnected && isBandSet;
        stopBreathingButton.interactable = isBeltConnected && isBandSet;
        wrapButton.interactable = isBeltConnected && isBandSet;
        unwrapButton.interactable = isBeltConnected && isBandSet;
    }

    /// <summary>
    /// Stop thread at end of simulation
    /// </summary>
    void OnDisable()
    {
        // The serialThread reference should never be null at this point,
        // unless an Exception happened in the OnEnable(), in which case I've
        // no idea what face Unity will make.
        if (motorThread != null)
        {
            motorThread.RequestStop();
            motorThread = null;
        }

        // This reference shouldn't be null at this point anyway.
        if (thread != null)
        {
            thread.Join();
            thread = null;
        }
    }



    /// <summary>
    /// Start thread at start of application
    /// </summary>
    public void Connect(string port, float length, float thickness)
    {
        motorThread = new MotorThread(
            port,
            length,
            thickness,
            Parameters.beltID,
            BAUDRATE,
            verbose,
            goalCurrentSet,
            goalCurrentWrappedMin,
            goalCurrentWrappedMax);
        thread = new Thread(new ThreadStart(motorThread.RunForever));
        thread.Start();
    }

    /// <summary>
    /// Set band (belt tightens until it detects a couple in the motor)
    /// </summary>
    public void SetBand()
    {
        motorThread.SendMessage(new MotorThread.CommandMsg('S'));
    }

    /// <summary>
    /// Switch to Wrap state from other script
    /// </summary>
    /// <param name="duration">Duration in s</param>
    /// <param name="intensity">Intensity (0-1)</param>
    public void TriggerWrap(double duration, float intensity)
    {
        motorThread.SendMessage(new MotorThread.CommandMsg('W', duration, intensity));
    }

    /// <summary>
    /// Switch to Unwrap state from other script
    /// </summary>
    /// <param name="duration">Duration in s</param>
    public void TriggerUnwrap(double duration)
    {
        motorThread.SendMessage(new MotorThread.CommandMsg('U', duration));
    }

    /// <summary>
    /// Send emergency message to motor (belt unwraps completely)
    /// </summary>
    public void Emergency()
    {
        motorThread.SendMessage(new MotorThread.CommandMsg('E'));
    }

    /// <summary>
    /// Belt is not connected : stop controller
    /// </summary>
    public void Stop()
    {
        motorThread.RequestStop();
    }

    /// <summary>
    /// Close thread and reconnect
    /// </summary>
    /// <param name="port"></param>
    /// <param name="length"></param>
    /// <param name="thickness"></param>
    public void Reconnect(string port, float length, float thickness)
    {
        Stop();
        Connect(port, length, thickness);
    }
}
