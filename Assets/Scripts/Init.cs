using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEditor;

public class Init : MonoBehaviour
{
    private bool startedEndRoutine = false;

    public BreathController breath;
    public MotorController belt;

    public Button setBeltButton;
    public InputField lengthField;
    public InputField thicknessField;
    public InputField beltPortField;

    void Start()
    {
        beltPortField.text = PlayerPrefs.GetString(Parameters.beltPortUI, "COM7");
        float length = PlayerPrefs.GetFloat(Parameters.bandLengthUI, 1.41f);
        float thickness = PlayerPrefs.GetFloat(Parameters.bandThicknessUI, 0.001f);
        lengthField.text = length.ToString();
        thicknessField.text = thickness.ToString();
        belt.Connect(beltPortField.text, length, thickness);
    }

    /// <summary>
    /// Start ending routine
    /// </summary>
    public void StartEnd()
    {
        // Quit application when over
        if (!startedEndRoutine)
        {
            SaveParams();
            startedEndRoutine = true;
            if (belt.isBeltConnected) belt.Emergency();
            else
            {
                #if UNITY_EDITOR
                EditorApplication.ExitPlaymode();
                #else
                Application.Quit();
                #endif
            }
        }
    }

    public void Reconnect()
    {
        belt.Reconnect(beltPortField.text, float.Parse(lengthField.text), float.Parse(thicknessField.text));
    }

    /// <summary>
    /// Save parameters
    /// </summary>
    private void SaveParams()
    {
        float bandLength = float.Parse(lengthField.text);
        float bandThickness = float.Parse(thicknessField.text);
        PlayerPrefs.SetString(Parameters.beltPortUI, beltPortField.text);
        PlayerPrefs.SetFloat(Parameters.bandLengthUI, bandLength);
        PlayerPrefs.SetFloat(Parameters.bandThicknessUI, bandThickness);
    }
}
