﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Main controller for rendering breathing parameters
/// </summary>
public class BreathController : MonoBehaviour
{
    #region Parameters
    // Constants
    private float almostOne = 0.95f;

    // Current parameters
    public float period = 4;
    private float depth = 0.5f;
    private float inspOvTot = 0.45f;

    // Loop
    private bool isPlaying = false;
    private float nextBreathTime;

    // GameObjects and Components
    public MotorController belt;
    public Slider strength;
    #endregion

    /// <summary>
    /// Called once per frame
    /// </summary>
    void Update()
    {
        if (isPlaying && Time.time > nextBreathTime)
        {
            // Update parameters
            depth = strength.value;

            // Slower or faster breathing
            nextBreathTime += period;

            float inspTime = period * inspOvTot * almostOne;
            float expTime = period * (1 - inspOvTot) * almostOne;
            float expDelay = period * inspOvTot;

            Inspire(inspTime, depth);
            Wait(expDelay, Expire, expTime, depth);
        }
    }

    /// <summary>
    /// Start playing breathing parameters
    /// </summary>
    public void Play()
    {
        isPlaying = true;
        nextBreathTime = Time.time;
    }

    /// <summary>
    /// Stop playing breathing
    /// </summary>
    public void Stop()
    {
        isPlaying = false;
    }

    /// <summary>
    /// Render inspiration
    /// </summary>
    /// <param name="inspTime">Duration of inspiration</param>
    /// <param name="depth">Depth of inspiration</param>
    private void Inspire(float inspTime, float depth)
    {
        if (belt != null)
        {
            belt.TriggerWrap(inspTime, depth);
        }
    }

    /// <summary>
    /// Render expiration
    /// </summary>
    /// <param name="expTime">Duration of expiration</param>
    /// <param name="depth">Depth of expiration</param>
    private void Expire(float expTime, float depth)
    {
        if (belt != null)
        {
             belt.TriggerUnwrap(expTime);
        }
    }

    /// <summary>
    /// Utilitary function for Coroutines
    /// </summary>
    /// <param name="delay">Delay before action</param>
    /// <param name="action">Action performed afetr delay</param>
    /// <param name="arg1">1st argument</param>
    /// <param name="arg2">2nd argument</param>
    protected void Wait(float delay, System.Action<float, float> action, float arg1, float arg2)
    {
        if (delay == 0) action.Invoke(arg1, arg2);
        else StartCoroutine(GenericCoroutine(delay, action, arg1, arg2));
    }

    /// <summary>
    /// Utilitary function for Coroutines
    /// </summary>
    /// <param name="delay">Delay before action</param>
    /// <param name="action">Action performed afetr delay</param>
    /// <param name="arg1">1st argument</param>
    /// <param name="arg2">2nd argument</param>
    /// <returns></returns>
    IEnumerator GenericCoroutine(float delay, System.Action<float, float> action, float arg1, float arg2)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke(arg1, arg2);
    }
}
