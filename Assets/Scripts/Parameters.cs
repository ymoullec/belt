using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Parameters
{
    public const byte beltID = 1;

    public const string beltPortUI = "menu.beltport";
    public const string bandLengthUI = "menu.bandlength";
    public const string bandThicknessUI = "menu.bandthickness";
}
